# Tutorías CINV: Python
Este repositorio contiene los archivos que serán utilizados en este instructivo. 
Es necesario instalar la biblioteca **spikelib**, se recomienda instalar el siguiente [fork]. Para esto clonar el repositorio e instalar directo desde la ruta donde ha sido descargado, por consola:

```
$ git clone https://github.com/pabloreyesrobles/spikelib.git
$ cd spikelib
$ pip install .
```

Instalar neuroshare:
```
$ git clone https://github.com/G-Node/python-neuroshare
$ cd python-neuroshare
$ pip install .
```

Además instalar los siguientes paquetes mediante pip
```
$ pip install tqdm h5py seaborn ipywidgets
```

Y ejecutar
```
$ jupyter nbextension enable --py widgetsnbextension
```


[fork]: https://github.com/pabloreyesrobles/spikelib/